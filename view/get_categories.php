<?php
$tags = Category::getAll();

header('Content-Type: application/json');
echo json_encode(array('tags' => $tags));