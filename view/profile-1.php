<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <title>Profile</title>
</head>
<body>
    <h1><?= $_SESSION["logged"]["name"] ?>'s Profile</h1>

    <b>For You</b>
    <table>
        <tr>
            <?php
                $id = $_SESSION["logged"]["id"];
    
                $arrForYou = Recom::getUsersForYou($id);
                foreach ($arrForYou as $h)
                {
                    $filmName = Film::getFromId($h['id']);
                    echo "
                        <td>
                            <form action='movie.php' method='get' name='{$filmName['id']}'>
                                <input type='hidden' value='{$h['id']}' name='film'>
                                <button style='border: none'  type=submit><img src='../admin/{$filmName['thumbnail']}' style='width: 200px'></button>
                            </form>
                        <td>
                    ";
                }

            ?>
        </tr>
    </table>
    <b>History</b>
    <table>
        <tr>
            <?php
                $id = $_SESSION["logged"]["id"];
                $arrHistory = History::getUsersHistory($id);
                foreach ($arrHistory as $h)
                {
                    $filmName = Film::getFromId($h['film']);
                    $minutes = floor($h['timestamp'] / 60);
                    $seconds = $h['timestamp'] % 60;
                    $timestamp = sprintf("%02d:%02d", $minutes, $seconds);
                    // {$filmName['title']} - {$timestamp} - {$h['last_updated']} 
                    echo "
                        <td>
                            <form action='movie.php' method='get' name='{$filmName['id']}'>
                                <input type='hidden' value='{$h['film']}' name='film'>
                                <button style='border: none'  type=submit><img src='../admin/{$filmName['thumbnail']}' style='width: 200px'></button>
                            </form>
                        <td>
                    ";
                }

            ?>
        </tr>
    </table>
    </br><b>Subscription</b>
    <?php
        $id = $_SESSION["logged"]["id"];
        if (Users::checkSubscription($id)){
            $userSub = Subs::getUsersSubscription($id);
            if ($userSub) {
                $subModel = Subs::getSub($userSub['sub_model']);
                    
                $now = time(); 
                $expDate = strtotime($userSub['exp_date']);
                $datediff = (round(($expDate - $now) / (60 * 60 * 24)));
            
                echo '</br> Subscription Model : ' . $subModel['name'];
                echo '</br> Purchased On : ' . $userSub['purchase_date'];
                echo '</br> Will Expire In : ' . $userSub['exp_date'] . " (" . $datediff ." Days)";

                if ($datediff < 8)
                {
                    echo "
                    <div>
                        </br> Your subscription will expire in {$datediff} day(s)
                        </br> Would you like to renew your subscription ?
                        <form method='post' action='payment.php'>
                            <button class='btn btn-warning'>Renew</button>
                        </form>
                    </div>
                    ";
                }
            }
        }
        else {
            echo '</br> Have not purchased a subscription.';
            echo '</br> <a href="payment.php">Click Here</a>';
        }
    ?>
</body>
    <script>
        
    </script>
</html>