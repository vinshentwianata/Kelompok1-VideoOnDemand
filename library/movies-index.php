<?php
$db = DB::getInstance();
$lists = $db->query("SELECT *
                    FROM LISTS
                    WHERE STATUS = 1
                    ORDER BY TIER
                        ")->fetchAll();

$display = [];
foreach ($lists as $l) {
    $id = $l["id"];
    $films = $db->query("SELECT DISTINCT F.*
                        FROM FILM F
                        WHERE F.id in (
                            SELECT FILM_ID
                            FROM LISTS_MEMBER
                            WHERE LISTS_ID = '$id'
                        )
                        ")->fetchAll();
    array_push($l,$films);
    array_push($display,$l);
}

array_walk($display, function (& $item) {
    $item['films'] = $item['0'];
    unset($item['0']);
    });

if ($lists) {
    header('Content-Type: application/json');
    echo json_encode($display);
} else {
    echo "No movies found";
}