<?php
class Lists {

    public static function getAll()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM LISTS ORDER BY TIER")->fetchAll();
        return $result;
    }

    public static function getAllVisible()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM LISTS WHERE STATUS = 1 ORDER BY TIER")->fetchAll();
        return $result;
    }
    
    public static function getFromId($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                            FROM LISTS
                            WHERE ID = ?");
        $stmt->execute([$id]);
        $result = $stmt->fetch();
        return $result;
    } 

    public static function updateList($id, $name, $desc)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("UPDATE LISTS
                            SET NAME = ?, DESCRIPTION = ?
                            WHERE ID = ?");
        $stmt->execute([$name, $desc, $id]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function getMembersFromId($id)
    {   
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                            FROM LISTS_MEMBER
                            WHERE LISTS_ID = ?");
        $stmt->execute([$id]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function generateId()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM LISTS ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],1,5); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 5)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "L" . $newId; ## tambah huruf
    }

    public static function generateTier()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT COUNT(*) + 1 AS ID FROM LISTS")->fetch();
        return $result["ID"];
    }

    public static function generateListId()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM LISTS_MEMBER ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],2,10); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 10)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "LM" . $newId; ## tambah huruf
    }

    public static function insert($name, $desc)
    {
        $db = DB::getInstance();
        $tier = self::generateTier();
        $id = self::generateId();
        $result = $db->prepare("INSERT INTO LISTS
                                VALUES (?, ?, ?, ?, 0)
                                ");
        $result = $result->execute([$id, $name, $desc, $tier]);
        return $result;
    }

    public static function turnVisible($id)
    {
        $db = DB::getInstance();
        $result = $db->prepare("UPDATE LISTS
                                SET STATUS = 1 WHERE ID = ?
                                ");
        $result = $result->execute([$id]);
        return $result;
    }

    public static function turnHidden($id)
    {
        $db = DB::getInstance();
        $result = $db->prepare("UPDATE LISTS
                                SET STATUS = 0 WHERE ID = ?
                                ");
        $result = $result->execute([$id]);
        return $result;
    }

    public static function setTier($id, $tier)
    {
        $db = DB::getInstance();
        $result = $db->prepare("UPDATE LISTS
                                SET TIER = ? WHERE ID = ?
                                ");
        $result = $result->execute([$tier, $id]);
        return $result;
    }

    public static function deleteLists($id)
    {
        $db = DB::getInstance();
        $result = $db->prepare("DELETE FROM LISTS
                                WHERE ID = ?
                                ");
        $result = $result->execute([$id]);
        return $result;
    }

    public static function insertInto($listId, $filmId)
    {
        $db = DB::getInstance();
        $id = self::generateListId();
        $result = $db->prepare("INSERT INTO LISTS_MEMBER
                                VALUES(?,?,?)
                                ");
        $result = $result->execute([$id, $listId, $filmId]);
        return $result;
    }

    public static function deleteFrom($id)
    {
        $db = DB::getInstance();
        $result = $db->prepare("DELETE FROM LISTS_MEMBER
                                WHERE ID = ?
                                ");
        $result = $result->execute([$id]);
        return $result;
    }
}
