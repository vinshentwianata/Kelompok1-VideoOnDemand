<?php
class Users {

    public static function getAll()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM USERS")->fetchAll();
        return $result;
    }

    public static function getAllSubscribed()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT DISTINCT * FROM USERS WHERE ID IN (SELECT USER FROM SUBSCRIPTION_USER)")->fetchAll();
        return $result;
    }

    public static function getFromId($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                              FROM USERS
                              WHERE ID = ?
                                ");
        $stmt->execute([$id]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function generateId()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM USERS ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],1,5); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 5)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "U" . $newId; ## tambah huruf
    }

    public static function checkTaken($name)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT * FROM USERS WHERE NAME=?");
        $stmt->execute([$name]); 
        $result = $stmt->fetch();
        return $result;
    }

    public static function cekEmailAda($email)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT * FROM USERS WHERE EMAIL=?");
        $stmt->execute([$email]); 
        $result = $stmt->fetch();
        return $result;
    }

    public static function insert($name,$email,$pass)
    {
        $db = DB::getInstance();
        $id = Users::generateId();
        $stmt = $db->prepare("INSERT INTO USERS VALUES (?, ?, ?, ?, '', 'Active')");
        $result = $stmt->execute([$id, $name, $email, $pass]);
        return $result;
    }

    public static function delete($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("DELETE FROM USERS WHERE ID = ?");
        $result = $stmt->execute([$id]);
        return $result;
    }

    public static function logging(string $username, string $password)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT * 
                                FROM USERS 
                            WHERE NAME=? AND PASS = ? AND STATUS = 'Active'");
        $stmt->execute([$username, $password]); 
        $result = $stmt->fetch();
        return $result;
    }

    public static function checkSubscription($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT * 
                                FROM USERS 
                                WHERE ID=? 
                                AND ID IN  (SELECT user FROM SUBSCRIPTION_USER WHERE EXP_DATE > {fn NOW()})");
        $stmt->execute([$id]); 
        $result = $stmt->fetch();
        return $result;
    }

    public static function getUsersSubscription($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT * 
                                FROM SUBSCRIPTION_USER 
                                WHERE USER=? 
                                ");
        $stmt->execute([$id]); 
        $result = $stmt->fetch();
        return $result;
    }

    public static function toggleStatus($idUser)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT * 
                                FROM USERS 
                                WHERE ID = ? 
                                ");
        $stmt->execute([$idUser]); 
        $result = $stmt->fetch();
        if ($result["status"] == "Active")
        {
            $stmt = $db->prepare("UPDATE USERS 
                                    SET STATUS = 'Inactive'
                                    WHERE ID=? 
                                ");
        }
        else if ($result["status"] == "Inactive")
        {
            $stmt = $db->prepare("UPDATE USERS 
                                    SET STATUS = 'Active'
                                    WHERE ID=? 
                                ");
        }
        
        $result = $stmt->execute([$idUser]); 
        return $result;
    }
}