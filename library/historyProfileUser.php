<?php
    
class HistoryUser{
    public function getUsersHistory($user)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT F.*
                                FROM HISTORY_USER H
                                JOIN FILM F ON F.ID = H.film
                                WHERE H.USER = ?
                                ORDER BY H.LAST_UPDATED DESC
                            ");                        
        $stmt->execute([$user]);
        $result = $stmt->fetchAll();
        if ($result) {
            // Return the movies array as JSON
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            echo "No movies found";
        }
        return $result;
    }

}

$id = $_SESSION["logged"]["id"];
$userHistory = new HistoryUser();
$userHistory->getUsersHistory($id);

?>