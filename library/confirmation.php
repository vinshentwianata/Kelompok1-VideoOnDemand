<?php
class Confirmation {

    public static function getAll()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM CONFIRMATION")->fetchAll();
        return $result;
    }

    public static function getConfirmationBetweenDates($startDate, $endDate)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                                FROM CONFIRMATION WHERE
                                DATE_CONFIRMED >= ? AND DATE_CONFIRMED <= ? 
                            ");                        
        $stmt->execute([$startDate, $endDate]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function cekNamaAda($nama)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                                FROM CONFIRMATION
                                WHERE USERNAME = ?
                                ");                        
        $stmt->execute([$nama]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function cekEmailAda($email)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                                FROM CONFIRMATION
                                WHERE EMAIL = ?
                                ");
        $stmt->execute([$email]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function generateId()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM CONFIRMATION ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],2,5); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 5)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "CO" . $newId; ## tambah huruf
    }

    public static function getFromCode($code)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                                FROM CONFIRMATION
                                WHERE code = ?
                                ");                        
        $stmt->execute([$code]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function insert($name, $email, $pass, $code)
    {
        $db = DB::getInstance();
        $id = self::generateId();
        $result = $db->prepare("INSERT INTO CONFIRMATION
                                (id, username, email, password, code, date_created)
                                VALUES (?, ?, ?, ?, ?, {fn NOW()})
                                ");
        $result = $result->execute([$id, $name, $email, $pass, $code]);
        return $result;
        
    }

    public static function delete($id)
    {   
        $db = DB::getInstance();
        $result = $db->prepare("DELETE FROM CONFIRMATION
                                WHERE ID = ?
                                ");
        $result = $result->execute([$id]);
        return $result;
    }

    public static function confirmFromEmail($email)
    {   
        $db = DB::getInstance();
        $result = $db->prepare("UPDATE CONFIRMATION 
                                SET date_confirmed = {fn NOW()}
                                WHERE EMAIL = ?
                                ");
        $result = $result->execute([$email]);
        return $result;
    }
}
