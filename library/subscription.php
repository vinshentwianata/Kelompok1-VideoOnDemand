<?php
class Subs {

    public static function getAll()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM SUBSCRIPTION_USER")->fetchAll();
        return $result;
    }

    public static function getModelFromId($id)
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM SUBSCRIPTION_MODEL WHERE ID = '$id'")->fetch();
        return $result;
    }

    public static function getSubscriptionBetweenDates($modelId, $startDate, $endDate)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                              FROM SUBSCRIPTION_USER WHERE
                              SUB_MODEL LIKE CONCAT('%',?,'%')
                              AND PURCHASE_DATE >= ? AND PURCHASE_DATE <= ? 
                            ");                        
        $stmt->execute([$modelId, $startDate, $endDate]);
        $result = $stmt->fetchAll();
        return $result;
    }


    public static function updateModel($id, $name, $price, $model)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("UPDATE SUBSCRIPTION_MODEL
                            SET NAME = ?, PRICE = ?, PRICING_MODEL = ?
                            WHERE ID = ?");
        $stmt->execute([$name, $price, $model, $id]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function getAllModel()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM SUBSCRIPTION_MODEL")->fetchAll();
        return $result;
    }

    public static function getModelsCount($modelId)
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT COUNT(*) AS NUMBER FROM SUBSCRIPTION_USER WHERE SUB_MODEL = '$modelId'")->fetch();
        return $result["NUMBER"];
    }

    public static function getModelsSubscribers($modelId)
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM SUBSCRIPTION_USER WHERE SUB_MODEL = '$modelId'")->fetchAll();
        return $result;
    }

    public static function getUsersSubscription($user)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                            FROM SUBSCRIPTION_USER
                            WHERE USER = ?");
        $stmt->execute([$user]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function getSub($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                            FROM SUBSCRIPTION_MODEL
                            WHERE ID = ?");
        $stmt->execute([$id]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function getModelDate($modelId)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT PRICING_MODEL
                            FROM SUBSCRIPTION_MODEL
                            WHERE ID = ?");
        $stmt->execute([$modelId]);
        $result = $stmt->fetch();
        $date = date('Y-m-d', strtotime("+".$result["PRICING_MODEL"]." days"));
        return $date;
    }

    public static function generateId()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM SUBSCRIPTION_USER ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],1,7); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 7)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "S" . $newId; ## tambah huruf
    }

    public static function generateIdModel()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM SUBSCRIPTION_MODEL ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],1,5); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 5)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "M" . $newId; ## tambah huruf
    }

    public static function insertModel($name, $price, $model) //bagian insert new subs
    {
        if ($name && $price && $model != "") {
            $db = DB::getInstance();
            $id = self::generateIdModel();
            $stmt = $db->prepare("INSERT INTO SUBSCRIPTION_MODEL 
                                VALUES (?,?,?,?)
                                ");
            $result = $stmt->execute([$id, $name, $price, $model]);
            return $result;
        }
        else{
            $_SESSION['msg'] = "Field tidak boleh kosong! ";
            return;
        }
    }

    public static function deleteModel($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("DELETE FROM SUBSCRIPTION_USER 
                              WHERE SUB_MODEL = ?
                            ");
        $stmt->execute([$id]);
        $stmt = $db->prepare("DELETE FROM SUBSCRIPTION_MODEL 
                              WHERE ID = ?
                            ");
        $result = $stmt->execute([$id]);
        return $result;
    }

    public static function insert($user, $model)
    {
        $db = DB::getInstance();
        $id = self::generateId();
        $date = self::getModelDate($model);
        $stmt = $db->prepare("INSERT INTO SUBSCRIPTION_USER
                              VALUES (?,?,?, {fn NOW()}, ?)
                            ");
        $result = $stmt->execute([$id, $model, $user ,$date]);
        return $result;
    }

    public static function renew($user, $model)
    {
        $db = DB::getInstance();

        $stmt = $db->prepare("SELECT PRICING_MODEL
                            FROM SUBSCRIPTION_MODEL
                            WHERE ID = ?");
        $stmt->execute([$model]);
        $extendedDays = $stmt->fetch();

        $stmt = $db->prepare("SELECT EXP_DATE
                            FROM SUBSCRIPTION_USER
                            WHERE USER = ?");
        $stmt->execute([$user]);
        $expireDate = $stmt->fetch();
        
        $expireDate= date_create($expireDate["EXP_DATE"]);
        date_add($expireDate,date_interval_create_from_date_string($extendedDays["PRICING_MODEL"]." days"));
        $date =  date_format($expireDate,"Y-m-d");
      

        $stmt = $db->prepare("UPDATE SUBSCRIPTION_USER
                              SET SUB_MODEL = ?, PURCHASE_DATE = {fn NOW()}, EXP_DATE = ?
                              WHERE USER = ?
                            ");

        $result = $stmt->execute([$model, $date, $user]);
        return $result;
    }

    public static function getmonth()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM SUBSCRIPTION_MODEL where name='Monthly'")->fetchAll();
        return $result;
    }
    public static function getyear()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM SUBSCRIPTION_MODEL where name='yearly'")->fetchAll();
        return $result;
    }

    public static function deleteSubs($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("DELETE FROM SUBSCRIPTION_USER 
                                WHERE id = ?
                            ");
        $result = $stmt->execute([$id]);
        return $result;
    }
}