<?php
class Recom {

    public static function generateId()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM RECOMMENDATION ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],1,7); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 7)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "R" . $newId; ## tambah huruf
    }

    public static function getUsersRecom(string $id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                            FROM RECOMMENDATION
                            WHERE USER = ?
                            LIMIT 8");
        $stmt->execute([$id]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function getUsersForYou(string $id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT F.*
                            FROM FILM F, RECOMMENDATION R
                            WHERE R.USER = ? AND R.FILM = F.ID
                            LIMIT 8");
        $stmt->execute([$id]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function setUsersRecom(string $userId, array $films) #user id, array of films
    {
        $db = DB::getInstance();
        $result = $db->prepare("DELETE 
                                FROM RECOMMENDATION
                                WHERE USER = ?");
        $result->execute([$userId]);

        foreach ($films as $f)
        {
            $id = self::generateId();
            $filmId = $f["ID"];
            $recom = $db->prepare("INSERT INTO RECOMMENDATION
                                    VALUES (?,?,?)
                                    ");
            $recom->execute([$id, $userId, $filmId]);
        }

        return $result;
    }

    public static function getRecomFromUsersHistory(string $id) #user id
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT C.ID as ID
                            FROM HISTORY_USER H, CATEGORY C, FILM_CATEGORY FC 
                            WHERE H.USER = ? AND FC.CATEGORY = C.ID AND FC.FILM = H.FILM
                            LIMIT 8");
        $stmt->execute([$id]);
        $categories = $stmt->fetchAll();

        $categoryQuery = "";
        foreach ($categories as $c)
        {
            $category = $c["ID"];
            $categoryQuery = $categoryQuery . " C.ID = '$category' OR ";
        }
        $categoryQuery = substr($categoryQuery, 0, -3);

        $stmt = $db->prepare("SELECT DISTINCT F.ID AS ID
                                FROM FILM F, FILM_CATEGORY FC, CATEGORY C 
                                WHERE F.ID = FC.FILM AND C.ID = FC.CATEGORY AND ($categoryQuery) 
                                AND F.ID NOT IN (
                                    SELECT FILM
                                    FROM HISTORY_USER
                                    WHERE USER = ?
                                )
                                LIMIT 8");
        $stmt->execute([$id]);
        $categories = $stmt->fetchAll();
        return $categories;
    }

    public static function getRecomFromCategories(array $categories) #array of category id
    {
        if (sizeof($categories) <= 0) return;
        $db = DB::getInstance();
        $result = [];
        $categoryQuery = "";
        foreach ($categories as $c)
        {
            $categoryQuery = $categoryQuery . " C.ID = '$c' OR";
        }
        $categoryQuery = substr($categoryQuery, 0, -3);

        $result = $db->query(  
            "SELECT DISTINCT F.ID AS ID
            FROM FILM F, FILM_CATEGORY FC, CATEGORY C 
            WHERE F.ID = FC.FILM AND C.ID = FC.CATEGORY AND ($categoryQuery)
            LIMIT 8"
        )->fetchAll();

        return $result;
    }

}