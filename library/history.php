<?php
class History {

    public static function getAll()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM HISTORY_USER")->fetchAll();
        return $result;
    }

    public static function getHistoryBetweenDatesByFilm($filmId, $startDate, $endDate)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                                FROM HISTORY_USER WHERE
                                FILM LIKE CONCAT('%',?,'%') AND
                                LAST_UPDATED >= ? AND LAST_UPDATED <= ? 
                            ");                        
        $stmt->execute([$filmId, $startDate, $endDate]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function getHistoryBetweenDatesByCategory($categoryId, $startDate, $endDate)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                                FROM HISTORY_USER WHERE
                                FILM IN 
                                ( SELECT FILM
                                    FROM FILM_CATEGORY
                                    WHERE CATEGORY LIKE CONCAT('%',?,'%')
                                )
                                AND LAST_UPDATED >= ? AND LAST_UPDATED <= ? 
                            ");                        
        $stmt->execute([$categoryId, $startDate, $endDate]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function getHistoryBetweenDatesByLists($listId, $startDate, $endDate)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                                FROM HISTORY_USER WHERE
                                FILM IN 
                                ( SELECT FILM_ID
                                    FROM LISTS_MEMBER
                                    WHERE LISTS_ID LIKE CONCAT('%',?,'%')
                                )
                                AND LAST_UPDATED >= ? AND LAST_UPDATED <= ? 
                            ");                        
        $stmt->execute([$listId, $startDate, $endDate]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function getUsersHistory($user)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                                FROM HISTORY_USER
                                WHERE USER = ?
                                ORDER BY LAST_UPDATED DESC
                            ");                        
        $stmt->execute([$user]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function getByUsersFilm($user, $film)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                                FROM HISTORY_USER
                                WHERE FILM = ? AND USER = ?
                            ");                        
        $stmt->execute([$film, $user]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function generateId()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM HISTORY_USER ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],1,7); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 7)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "H" . $newId; ## tambah huruf
    }

    public static function insert($user, $film, $timestamp, $watched)
    {
        $db = DB::getInstance();
        $id = self::generateId();
        $result = $db->prepare("INSERT INTO HISTORY_USER
                                VALUES (?, ?, ?, ?, ?, {fn NOW()})
                                ");
        $result = $result->execute([$id, $user, $film, $timestamp, $watched]);
        return $result;
    }

    public static function setNewTimestamp($id, $timestamp)
    {
        $db = DB::getInstance();
        $result = $db->prepare("UPDATE HISTORY_USER
                                SET TIMESTAMP = ?, LAST_UPDATED = {fn NOW()}
                                WHERE ID = ?
                                ");
        $result = $result->execute([$timestamp, $id]);
        return $result;
    }

    public static function setWatched($id, $watched)
    {
        $db = DB::getInstance();
        $id = self::generateId();
        $result = $db->prepare("UPDATE HISTORY_USER
                                SET WATCHED = ?, LAST_UPDATED = {fn NOW()}
                                WHERE ID = ?
                                ");
        $result = $result->execute([$watched, $id]);
        return $result;
    }
}