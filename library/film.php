<?php
class Film {

    public static function getAll()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM FILM")->fetchAll();
        return $result;
    }

    public static function getGlobalWatchtime()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT SUM(timestamp) as NUMBER FROM HISTORY_USER")->fetch();
        return $result["NUMBER"];
    }

    public static function getAllNotInCategory($categoryId)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                              FROM FILM WHERE
                              ID NOT IN (
                                        SELECT FILM
                                        FROM FILM_CATEGORY
                                        WHERE CATEGORY = ?
                                        );
                            ");                        
        $stmt->execute([$categoryId]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function getAllInCategory($categoryId)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                              FROM FILM WHERE
                              ID IN (
                                        SELECT FILM
                                        FROM FILM_CATEGORY
                                        WHERE CATEGORY = ?
                                        );
                            ");                        
        $stmt->execute([$categoryId]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function getFilmsCategory($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                              FROM CATEGORY WHERE
                              ID IN (
                                        SELECT CATEGORY
                                        FROM FILM_CATEGORY
                                        WHERE FILM = ?
                                        );
                            ");                        
        $stmt->execute([$id]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function getFilmsLists($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                              FROM LISTS WHERE
                              ID IN (
                                        SELECT LISTS_ID
                                        FROM LISTS_MEMBER
                                        WHERE FILM_ID = ?
                                        );
                            ");                        
        $stmt->execute([$id]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function getAllNotInLists($listsId)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                              FROM FILM WHERE
                              ID NOT IN (
                                        SELECT FILM_ID
                                        FROM LISTS_MEMBER
                                        WHERE LISTS_ID = ?
                                        );
                            ");                        
        $stmt->execute([$listsId]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function searchKeyword($keyword) 
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                              FROM FILM
                              WHERE TITLE LIKE '%?%'
                            ");                        
        $stmt->execute([$keyword]);
        $result = $stmt->fetchAll();
        return $result;
    }

    public static function getFromId($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT *
                            FROM FILM
                            WHERE ID = ?");
        $stmt->execute([$id]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function getViewsFromId($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT COUNT(H.ID) as NUMBER 
                              FROM HISTORY_USER H
                              WHERE H.FILM = ?
                            "
                              );
        $stmt->execute([$id]);
        $result = $stmt->fetch();                   
        return $result["NUMBER"];
    }

    public static function getWatchtimeFromId($id)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT IFNULL(SUM(H.TIMESTAMP),0) as NUMBER 
                              FROM HISTORY_USER H
                              WHERE H.FILM = ?
                            "
                              );
        $stmt->execute([$id]);
        $result = $stmt->fetch();    
        return $result["NUMBER"];
    }

    public static function generateId()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM FILM ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],1,5); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 5)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "F" . $newId; ## tambah huruf
    }

    public static function generateToken($userid, $filmid)
    {
        $db = DB::getInstance();
        $value = $userid.$filmid;
        $token = password_hash($value, PASSWORD_BCRYPT);

        return $token;
    }

    public static function insert($title,$description,$age_rating,$date,$link,$thumbnail,$imdb,$score)
    {
        $db = DB::getInstance();
        $id = self::generateId();
        $result = $db->prepare("INSERT INTO FILM
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
                                ");
        $result = $result->execute([$id, $title, $description, $age_rating, $date, $link, $thumbnail, $imdb, $score]);
        return $result;            

    }

    public static function update($id,$title,$description,$age_rating,$date,$link,$thumbnail,$imdb,$score)
    {
        $db = DB::getInstance();
        $result = $db->prepare("UPDATE FILM
                                SET TITLE = ?, DESCRIPTION = ?, AGE_RATING = ?, RELEASE_DATE = ?, LINK = ?, THUMBNAIL = ?, IMDB = ?, SCORE = ? 
                                WHERE ID = ?");
        $result = $result->execute([$title, $description, $age_rating, $date, $link, $thumbnail, $imdb, $score, $id]);
        return $result;
    }

    public static function delete($id)
    {
        $db = DB::getInstance();

        $result = $db->prepare("DELETE 
                                FROM LISTS_MEMBER
                                WHERE FILM_ID = ?");
        $result = $result->execute([$id]);

        $result = $db->prepare("DELETE 
                                FROM FILM_CATEGORY
                                WHERE FILM = ?");
        $result = $result->execute([$id]);

        $result = $db->prepare("DELETE 
                                FROM FILM
                                WHERE ID = ?");
        $result = $result->execute([$id]);

        return $result;
    }
}
