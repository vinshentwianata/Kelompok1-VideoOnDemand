<?php
class Category {

    public static function getAll()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM CATEGORY")->fetchAll();
        return $result;
    }

    public static function getFromId($id)
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM CATEGORY WHERE ID = '$id'")->fetch();
        return $result;
    }

    public static function getViewsFromId($id)
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT COUNT(H.ID) as NUMBER 
                              FROM HISTORY_USER H, FILM F, FILM_CATEGORY FC, CATEGORY C
                              WHERE C.ID = '$id' 
                                    AND FC.CATEGORY = C.ID
                                    AND F.ID = FC.FILM
                                    AND H.FILM = F.ID
                                    "
                              )->fetch();
        return $result["NUMBER"];
    }

    public static function getWatchtimeFromId($id)
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(SUM(H.TIMESTAMP),0) as NUMBER 
                              FROM HISTORY_USER H, FILM F, FILM_CATEGORY FC, CATEGORY C
                              WHERE C.ID = '$id'
                                    AND FC.CATEGORY = C.ID
                                    AND F.ID = FC.FILM
                                    AND H.FILM = F.ID
                                    "
                              )->fetch();
        return $result["NUMBER"];
    }

    public static function getFilmsFromId($id)
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT F.* 
                            FROM FILM F, FILM_CATEGORY FC 
                            WHERE FC.CATEGORY = '$id' AND F.ID = FC.FILM"
                            )->fetchAll();
        return $result;
    }

    public static function getCountMovies($idCategory)
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT COUNT(*) as NUMBER FROM FILM_CATEGORY WHERE CATEGORY = '$idCategory'")->fetch();
        return $result["NUMBER"];
    }

    public static function updateCategory($id, $name)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("UPDATE CATEGORY
                            SET NAME = ?
                            WHERE ID = ?");
        $stmt->execute([$name, $id]);
        $result = $stmt->fetch();
        return $result;
    }

    public static function generateId()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM CATEGORY ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],1,5); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 5)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "C" . $newId; ## tambah huruf
    }

    public static function generateIdEntry()
    {
        $db = DB::getInstance();
        $result = $db->query("SELECT IFNULL(ID,0) AS ID FROM FILM_CATEGORY ORDER BY ID DESC")->fetch(); ## ambil id paling tinggi
        $newId = substr($result["ID"],1,7); ## potong jadi angka saja
        $newId = (int) $newId; ## jadikan int
        $newId++; ## increment 1+
        while (strlen($newId) < 7)
        {
            $newId = "0" . $newId; ## tambah 0 sampai total digit ada 5
        }
        return "C" . $newId; ## tambah huruf
    }

    public static function insert($name)
    {
        if($name != ""){
            $db = DB::getInstance();
            $id = self::generateId();
            $result = $db->prepare("INSERT INTO CATEGORY
                                    VALUES (?, ?)
                                    ");
            $result = $result->execute([$id, $name]);
            return $result;
        }
        else{
            $_SESSION['msg'] = "Field tidak boleh kosong! ";
            return;
        }
    }

    public static function insertCategory($film, $category)
    {
        $db = DB::getInstance();
        $id = self::generateIdEntry();
        if (!self::checkHasCategory($film, $category))
        {
            $result = $db->prepare("INSERT INTO FILM_CATEGORY 
                                    VALUES (?, ?, ?)
                                    ");
            $result = $result->execute([$id, $category, $film]);
            return $result;
        }
    }

    public static function checkHasCategory($film, $category)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT COUNT(*) AS AMOUNT
                                FROM FILM_CATEGORY
                                WHERE CATEGORY = ? AND FILM = ?
                                ");
        $stmt->execute([$film, $category]);
        $result = $stmt->fetch();
        return $result["AMOUNT"];
    }

    public static function nameTaken($name)
    {
        $db = DB::getInstance();
        $stmt = $db->prepare("SELECT * FROM CATEGORY WHERE NAME=?");
        $stmt->execute([$name]); 
        $result = $stmt->fetch();
        return $result;
    }

    public static function clearFilmsCategory($film)
    {   
        $db = DB::getInstance();
        $result = $db->prepare("DELETE FROM FILM_CATEGORY
                                WHERE FILM = ?
                                ");
        $result = $result->execute([$film]);
        return $result;
    }

    public static function removeFromCategory($film, $category)
    {   
        $db = DB::getInstance();
        $result = $db->prepare("DELETE FROM FILM_CATEGORY
                                WHERE FILM = ? AND CATEGORY = ?
                                ");
        $result = $result->execute([$film, $category]);
        return $result;
    }

    public static function delete($id)
    {   
        $db = DB::getInstance();
        $result = $db->prepare("DELETE FROM FILM_CATEGORY
                                WHERE CATEGORY = ?
                                ");
        $result = $result->execute([$id]);
        $result = $db->prepare("DELETE FROM CATEGORY
                                WHERE ID = ?
                                ");
        $result = $result->execute([$id]);
        return $result;
    }
}   