
# Situs Video on Demand




![Logo](https://i.imgur.com/JyamFdr.png)


## Authors

- [Stevanus Fernandes Hia 221180545](https://git.sib.stts.edu/hiyaa)
- [Jesica Patricia Darsono 221180538](https://git.sib.stts.edu/jesica_patriciaa)
- [Steven Kim Wijaya 221180546](https://git.sib.stts.edu/stevzkw)
- [Vinshent Lauwrentz Wianata 221180547](https://git.sib.stts.edu/vinshent)


## Run Locally

Clone the project

```bash
  git clone https://git.sib.stts.edu/APLIN2023/Kelompok1-VideoOnDemand.git
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  npm install
```

React Package

```bash
  npm run watch
```

Composer packages

```bash
  composer require hidehalo/nanoid-php
  composer require midtrans/midtrans-php
  composer require phpmailer/phpmailer
  composer dump-autoload
```
