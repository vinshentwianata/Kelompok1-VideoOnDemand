<?php
if (isset($_SESSION['msg'])) {
        $msg = $_SESSION['msg'];
?>
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: '<?= $msg ?>',
        })
    </script>
<?php
    unset($_SESSION['msg']); 

    }
?>